package com.mtihc.minecraft.treasurechest.v8.core;

import com.mtihc.minecraft.treasurechest.v8.util.YamlFile;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.List;

public class TreasureManagerConfiguration extends YamlFile implements
        ITreasureManagerConfiguration {

    public TreasureManagerConfiguration(JavaPlugin plugin, String name) {
        super(plugin, name);
    }

    @Override
    public boolean getDefaultIgnoreProtection() {
        return getConfig().getBoolean("defaults.ignoreProtection");
    }

    @Override
    public int getSubregionSize() {
        return getConfig().getInt("rewards.restore.subregion-size", 50);
    }

    @Override
    public int getSubregionTicks() {
        return getConfig().getInt("rewards.restore.subregion-ticks", 10);
    }

    @Override
    public List<String> getRanks() {
        return getConfig().getStringList("ranks");
    }
}
